[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/rdubwiley%2Fcovid19_exponential_model/master)

Heavily inspired by Thomas Wiecki's work: [here](https://github.com/twiecki/covid19)
